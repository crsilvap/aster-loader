import cv2
import numpy as np
from sklearn.feature_extraction import image
from sklearn.cluster import spectral_clustering


def spectral_segmentation(img, n_clusters=5, **kwargs):
    mask = img.astype(bool)
    
    graph = image.img_to_graph(img, mask=mask)
    graph.data = np.exp(-graph.data / graph.data.std())

    labels = spectral_clustering(graph, n_clusters=n_clusters, eigen_solver='arpack')
    label_img = -np.ones(mask.shape)
    label_img[mask] = labels

    return label_img


def watershed_segmentation(gray, **kwargs):
    assert gray.ndim == 2

    img = np.tile(gray[:,:,None], (1, 1, 3))
    ret, thresh = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)

    # noise removal
    kernel  = kwargs.get('kernel', np.ones((3, 3), np.uint8))
    opening_iters = kwargs.get('opening_iters', 2)
    opening = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, kernel, iterations=opening_iters)

    # sure background area
    dilation_iters = kwargs.get('dilation_iters', 3)
    sure_bg = cv2.dilate(opening, kernel, iterations=dilation_iters)

    # finding sure background area
    dist_tf = cv2.distanceTransform(opening, cv2.DIST_L2, 5)
    ret, sure_fg = cv2.threshold(dist_tf, 0.7*dist_tf.max(), 255, 0)

    # finding unknown region
    sure_fg = np.uint8(sure_fg)
    unknown = cv2.subtract(sure_bg, sure_fg)

    # marker labelling
    ret, markers = cv2.connectedComponents(sure_fg)

    # add one to all labels so that sure background is not 0, but 1
    markers = markers + 1

    # now, mark the region of unknown with zero
    markers[unknown==255] = 0
    markers = cv2.watershed(img, markers)
    img[markers==-1] = [255, 0, 0]

    return img, markers

