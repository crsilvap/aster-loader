import numpy as np
import tables as pt
from skimage.transform import resize

class AsterImage():
    def __init__(self, h5file_path):
        self.path = h5file_path
        self.band_ranges = ['VNIR', 'TIR', 'SWIR']
        self.band_nodes = self._get_band_nodes()
        self.bands = {
            'VNIR': ['1', '2', '3N', '3B'],
            'SWIR' : ['4', '5', '6', '7', '8', '9'],
            'TIR': ['10', '11', '12', '13', '14']
        }
        self.node_keys = ['supplement', 'img_data', 'geo_data']

    @staticmethod
    def _get_band_nodes():
        band_nodes = {}
        for band_range in ['VNIR', 'SWIR', 'TIR']:
            nodes = {
                'root': f'/{band_range}',
                'supplement': f'/{band_range}/{band_range}_Supplement',
                'img_data': f'/{band_range}/{band_range}_Swath/Data Fields',
                'geo_data': f'/{band_range}/{band_range}_Swath/Geolocation Fields',
            }
            band_nodes[band_range] = nodes
        return band_nodes

    def load_image(self, band_range, n):
        """Returns a WxH image from a specific band-range. """

        with pt.open_file(self.path) as h5file:
            img = h5file.get_node(self.band_nodes[band_range]['img_data'], f'ImageData{n}').read()
        return img


    def load_band_cube(self, band_range):
        """Returns a WxHxB with B being the number of bands for a specific band-range. """

        imgs = []
        with pt.open_file(self.path) as h5file:
            for band in self.bands[band_range]:
                if band is not '3B':
                    img = h5file.get_node(self.band_nodes[band_range]['img_data'], f'ImageData{band}').read()
                    imgs.append(img)
        
        return np.stack(imgs, axis=-1)


    def load_flat_cube(self, band_range):
        """Returns a (W*H)xB with B being the number of bands for the specific band-range."""

        cube = self.load_band_cube(band_range)
        return np.reshape(cube, (cube.shape[0]*cube.shape[1], cube.shape[2]))


    def load_full_cube(self):
        """Resizes all band-ranges to a common (smallest) ground """
        vnir_cube = self.load_band_cube('VNIR')
        swir_cube = self.load_band_cube('SWIR')
        tir_cube  = self.load_band_cube('TIR')

        vnir_new_shape = [tir_cube.shape[0], tir_cube.shape[1], vnir_cube.shape[2]]
        swir_new_shape = [tir_cube.shape[0], tir_cube.shape[1], swir_cube.shape[2]]
    
        resized_vnir_cube = resize(vnir_cube, vnir_new_shape, mode='constant')
        resized_swir_cube = resize(swir_cube, swir_new_shape, mode='constant')

        return np.concatenate((resized_swir_cube, resized_vnir_cube, tir_cube), axis=-1)


    def load_coordinates(self, band_range):
        with pt.open_file(self.path) as h5file:
            lat = h5file.get_node(self.band_nodes[band_range]['geo_data'], 'Latitude').read()
            lng = h5file.get_node(self.band_nodes[band_range]['geo_data'], 'Longitude').read()
        return lat, lng


    def load_supplement(self, band_range):
        with pt.open_file(self.path) as h5file:
            node = h5file.get_node(self.band_nodes[band_range]['supplement'])
            if type(node) is pt.group.Group:
                arrays = {}
                for array in node:
                    arrays[array.name] = array.read()
                print('Returning dict of supplements.')
                return arrays
            elif type(node) is pt.array.Array:
                print('Returning array of supplement.')
                return node.read()