import matplotlib.pyplot as plt
import numpy as np

def plot_embedding(X, data, target, title=None):
    """Plot embedding results from sklearn.
    
    :param X:      embedding result from sklearn.manifold
    :param data:   original data used for embedding (NxD)
    :param target: target labels of the data (N)
    :param title:  (optional) title for the scatter plot
    :return:       None
    """
    x_min, x_max = np.min(X, 0), np.max(X, 0)
    X = (X - x_min) / (x_max - x_min)

    plt.figure()
    ax = plt.subplot(111)
    plt.scatter(X[:,0], X[:, 1], c=target, cmap=plt.cm.jet)
    for i in range(X.shape[0]):
        plt.text(X[i, 0] + 1e-2, X[i, 1] + 1e-2, str(target[i]+1),
                 color=plt.cm.jet(target[i]),
                 fontdict={'weight': 'bold', 'size': 9})

    if title is not None:
        plt.title(title)
